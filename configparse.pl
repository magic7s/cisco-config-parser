#!/usr/bin/perl 
#################################################
#		Cisco Config Parser		#
#   Written by: Brad Downey(brad@magic7s.com)	#
#						#
#		Version .20			#
#						#
#	This is licensed under the GPL		#
#						#
#						#
# The purpose of this script is to pull out 	#
# some of the useful data in a Cisco Config 	#
# file.  The default output format is in the 	#
# style my company's database system can import.#
# The output section is in the main program 	#
# and can be easily changed.			#
#						#
# There is SOME debugs you can do.  Just set 	#
# the line $DEBUG =0; to $DEBUG =1; and it will #
# dump a lot of data.				#
#						#
# Please if you have any suggestion or comments #
# e-mail me.					#
#						#
# Thanks and enjoy.				#
#################################################
$DEBUG =0;


#--------------- Subroutines ----------------

#--------- Reads in the config via an array and returns an array of hashes for all interfaces
sub findint (@) {
print "\nSubroutine findint started\n" if $DEBUG;
my @cfg = @_;
my @interfaces = ();
my $line = "";
my $intnum = 0;
my $intinfo = {};
foreach $line (@cfg) {
	chomp $line;
	print "Line is $line\n" if $DEBUG;
	if ($line =~ m/^(interface)\s*(\S*)(.*)$/) {
		push @interfaces, $intinfo;
		$intinfo = {};
		$intinfo->{'intname'} = $2;
		}
	if ($line =~ m/^(\s*ip address )(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})\s*(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})(.*)$/) {
		push @{$$intinfo{'ipaddress'}}, $2;
		push @{$$intinfo{'subnetmask'}}, $3;
 
		}
	if ($line =~ m/^(\s*description)\s*(.*)$/) {
		$intinfo->{'description'} = $2;
		}
	if ($line =~ m/^(\s*encapsulation\s*)(.*)$/) {
		if ($2 =~ m/.*frame-relay.*/) {
			$intinfo->{'encapsulation'} = "Frame-Relay"; }
		}
}
push @interfaces, $intinfo;

print "\nSubroutine findint executed\n" if $DEBUG;
shift @interfaces;
return @interfaces;
}

#--------- Reads in the config via an array and returns a scaler with hostname
sub findhostname (@) {
print "Subroutine findhostname started\n" if $DEBUG;
my @cfg = @_;
my $hostname = "";
my $line = "";

foreach $line (@cfg) {
chomp $line;
print "The line is: ", $line, "\n" if $DEBUG;
if ($line =~ m/^(hostname)\s(.*)$/) {
	$hostname = $2;
	print "Hostname is: ", $hostname, "\n" if $DEBUG;
	return $hostname;
}
}
print "Subroutine findhostname did not find hostname\n" if $DEBUG;
return 0;
}

#--------- Reads in the config via an array and returns a hash with con, aux, vty and enable passwords
sub findpasswd (@) {
print "\nSubroutine findpasswd started\n" if $DEBUG;
my @cfg = @_;
my %passwords = (
	vty => "",
	con => "",
	aux => "",
	enable => ""
);
my $line = "";
my $store = "";

foreach $line (@cfg) {
chomp $line;
print "The line is $line\n" if $DEBUG;

if ($line =~ m/^(enable\s\S*\s)(\d?)\s?(.*)$/) {
	if ($2 == "7") {$passwords{enable} = decrypt ($3);}
	else {
	$passwords{enable} = $3;}
	print "enablepass is $passwords{enable}\n" if $DEBUG;
}
if ($line =~ m/^(\s*snmp-server\s*community\s*)(\S*\s*R[OW]).*$/) {
	$passwords{'snmp'} = $2;
	}
if ($line =~ m/^(line con .*)$/) {
	$store = "con";
}
if ($line =~ m/^(line aux .*)$/) {
	$store = "aux";
}
if ($line =~ m/^(line vty .*)$/) {
        $store = "vty";
}

if ($line =~ m/^(\s*password\s)(\d?)\s?(.*)$/) {
	if ($2 == "7") {$passwords{$store} = decrypt ($3);}
	else {
	$passwords{$store} = $3;}
	$store = "";
	print "Password is  $passwords{$store}\n" if $DEBUG;
}

}
print "Subroutine findpasswd executed\n" if $DEBUG;
return %passwords;
}


#---- This subroutine decrypts Cisco Level 7 passwords
sub decrypt ($) {
my $s = "";
my $e = "";
my $ep = "";
my $i = "";
my @xlat = ( 0x64, 0x73, 0x66, 0x64, 0x3b, 0x6b, 0x66, 0x6f, 0x41,
          0x2c, 0x2e, 0x69, 0x79, 0x65, 0x77, 0x72, 0x6b, 0x6c,
          0x64, 0x4a, 0x4b, 0x44, 0x48, 0x53 , 0x55, 0x42 );
my $passstring = @_[0];
        $ep = $passstring; $dp = "";
        ($s, $e) = ($passstring =~ /^(..)(.+)/o);
        for ($i = 0; $i < length($e); $i+=2) {
                $dp .= sprintf "%c",hex(substr($e,$i,2))^$xlat[$s++];
                }
	print "The Encrypted String is $ep\nThe Decrypted String is $dp\n" if $DEBUG;
        return $dp;
}

#---- This subroutine finds and stores access-lists in a hash of arrays
sub findaccesslist (@) {
my @cfg = @_;
my $line = "";
my %accesslists;
foreach $line (@cfg) {
	chomp $line;
	if ($line =~ m/^(\s*access-list\s*)(\d{1,5}).*$/) {
	print "$line\n" if $DEBUG;
	print "$2\n" if $DEBUG;
	print "The keys are ", (keys %accesslists), "\n" if $DEBUG;
	push @{$accesslists{$2}}, $line;
	}
}
return %accesslists;
}

#---- This subroutine finds and stores the routing protocol information
sub findrouting (@) {
my @cfg = @_;
my $line = "";
my @protos;
foreach $line (@cfg) {
 chomp $line;
 if ($line =~ m/^\s*(router)\s(eigrp)\s(\d*).*$/) {
	print "$line\n" if $DEBUG;
	push @protos, "IP-EIGRP";
	}
 if ($line =~ m/^\s*(router)\s(ospf)\s(\d*).*$/) {
	print "$line\n" if $DEBUG;
	push @protos, "IP-OSPF";
	}
 if ($line =~ m/^\s*(router)\s(rip).*$/) {
	print "$line\n" if $DEBUG;
	push @protos, "IP-RIP";
	}
 if ($line =~ m/^\s*(router)\s(igrp)\s(\d*).*$/) {
	print "$line\n" if $DEBUG;
	push @protos, "IP-IGRP";
	}
 if ($line =~ m/^\s*(router)\s(bgp)\s(\d*).*$/) {
	print "$line\n" if $DEBUG;
	push @protos, "IP-BGP";
	}


}
return @protos;
}


#---- This subroutine finds and stores the static routes
sub findstatic (@) {
my @cfg = @_;
my $line = "";
my @routes;
foreach $line (@cfg) {
 chomp $line;
 if ($line =~ m/^\s*(ip\s+route\s+.*)$/) {
	print "$line\n" if $DEBUG;
	push @routes, $1;
	}

}
return @routes;
}


#--------------- Main Program ---------------
die "Usage: $0 configfile\n" unless (@ARGV);

$configfile = $ARGV[0];

open(CFG_FILE, $configfile) or die "Can not open file, $!";
while (<CFG_FILE>) {
push @org_config, $_;
}
close (CFG_FILE) or die "Can not close file correctly";


print "Dump of $configfile\n\n", @org_config, "\n" if $DEBUG;

$hostname = findhostname (@org_config);
print "Device,$hostname,";

%password = findpasswd (@org_config);
print "$password{con},";
print "$password{vty},";
print "$password{aux},";
print "$password{enable},";
print "$password{snmp}\n";

@interfaces = findint (@org_config);
foreach $int (@interfaces) {
print "Interface,$$int{'intname'},";
print "$$int{'encapsulation'}\n";
#print "Description is $$int{'description'}\n";
if (@{$$int{'ipaddress'}}) {
foreach $n (0..(@{$$int{'ipaddress'}} - 1)) {
	print "IP,${$$int{'ipaddress'}}[$n],";
	print "${$$int{'subnetmask'}}[$n]\n";
}
}

}

%acl = findaccesslist (@org_config);
foreach $key (keys %acl) {
	print "Access List,$key,";
	foreach $aclline (@{$acl{$key}}) {
	print "$aclline~";}
print "\n";
}

@routingproto = findrouting (@org_config);
foreach $proto (@routingproto) {
print "Routing Protocol,$proto\n";
}

@staticroutes = findstatic (@org_config);
if (@staticroutes) {
print "Routing Protocol,IP-STATIC,";
foreach $sroute (@staticroutes) {
	print "$sroute~";}
print "\n";
}


#------------End Main Program--------------------
